# hello-world

A simple, stupid testing/demo program. Only here so I can test git/gitlab/build features/workflows.

* The project has a Makefile which builds the program and which can optionally
  test to verify that it works.

* Every commit triggers the CI process to build the program and run tests with
  multiple (currently 3) compilers.

* TODO: I'd like to have Gitlab's CI build packages.
