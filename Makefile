CFLAGS ?= -Wall -Wextra -Werror -O2

all: hello

hello: hello.c
	$(CC) $(CFLAGS) hello.c -o hello

clean:
	rm -f hello

test: hello hello.output
	./hello 2>&1 | diff -q hello.output - >/dev/null || \
	(echo "Testing failed" && exit 1)
